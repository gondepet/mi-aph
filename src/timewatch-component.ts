import * as ECSA from '../libs/pixi-component';
import { RectangleCollider } from './abstracts/rectangle-collider';
import { Names, ComponentNames, BFlags, Messages, Attributes } from './constants';
import Dynamics from './abstracts/dynamics';
import PlayerInputController from './player-controller-component';
import GameFactory from './game-factory';
import { GameModel } from './game-model';

export class Timewatch extends ECSA.Component {

    model: GameModel;
    factory: GameFactory;
    timeElapsed: number;

    finished: number;
    started: number;
    gameStarted: boolean;

    constructor() {
        super()
    }
    onInit() {
        super.onInit();
        this.subscribe(Messages.GAME_OVER, Messages.GAME_START);
        this.model = this.scene.getGlobalAttribute<GameModel>(Attributes.GAME_MODEL);
        this.factory = this.scene.getGlobalAttribute<GameFactory>(Attributes.GAME_FACTORY);
        this.gameStarted = false;
    }

    onUpdate(delta: number, absolute: number) {
        if (this.gameStarted === true && this.started === undefined)
            this.started = absolute
        if (this.started !== undefined && this.gameStarted) {
            this.timeElapsed = absolute;
            this.owner.asText().text = "Time elapsed: " + ((this.timeElapsed - this.started) / 1000).toFixed(2) + " s";
        }
    }

    onMessage(msg: ECSA.Message) {
        if (msg.action === Messages.GAME_OVER && this.finished === undefined) {
            this.finished = this.timeElapsed - this.started;
            this.factory.gameFinished((this.finished / 1000).toFixed(2) + " s", this.owner);
            this.gameStarted = false;
            this.sendMessage(Messages.GAME_OVER);
        }
        if (msg.action === Messages.GAME_START)
            this.gameStarted = true;
    }

}
