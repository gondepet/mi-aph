import * as ECSA from '../../libs/pixi-component';
import { Names, ComponentNames, Attributes, GRAVITY_MODIFIER, ACCELERATION_MODIFIER } from '../constants';
import { GameModel } from '../game-model';
import GameFactory from '../game-factory';
import { CollisionManager } from '../colision-manager';
import { throwStatement } from 'babel-types';
import { RectangleCollider } from './rectangle-collider';


export default class Dynamics extends ECSA.Component {

    public aceleration: ECSA.Vector;
    private _velocity: ECSA.Vector;
    get velocity(): ECSA.Vector {
        return this._velocity;
    };

    get position(): ECSA.Vector {
        return new ECSA.Vector(this.owner.pixiObj.position.x,
            this.owner.pixiObj.position.y);
    };
    public disabled: boolean;
    public gravity: boolean
    private _gravity: number;
    private _force: ECSA.Vector;
    acelerationOrig: ECSA.Vector;
    velocityOrig: ECSA.Vector;
    collisionManager: CollisionManager;

    model: GameModel;
    factory: GameFactory;

    constructor(aceleration: ECSA.Vector, velocity: ECSA.Vector) {
        super();
        this.acelerationOrig = aceleration;
        this.velocityOrig = velocity;
        this._name = ComponentNames.DYNAMICS;
    }

    onInit() {
        super.onInit();
        this.model = this.scene.getGlobalAttribute<GameModel>(Attributes.GAME_MODEL);
        this.factory = this.scene.getGlobalAttribute<GameFactory>(Attributes.GAME_FACTORY);
        this.collisionManager = this.scene.getGlobalAttribute<CollisionManager>(Attributes.COLLISION_MANAGER);
        this._gravity = this.model.gravity;
        this.aceleration = this.acelerationOrig;
        this._velocity = this.velocityOrig;
        this._velocity.y += this._gravity * GRAVITY_MODIFIER;
        this._force = new ECSA.Vector(0, 0);
    }

    onUpdate(delta: number, absolute: number) {
        if (!this.disabled) {
            this.applyVelocity(delta);
            let deltaPos = this.calcPositionChange(delta);
            let oldPosX = this.owner.pixiObj.position.x;
            this.owner.pixiObj.position.x += deltaPos.x;
            let collider = this.collisionManager.collidePhysics(this.owner)
            if (collider != null) {
                if (deltaPos.x > 10e-6)
                    this.owner.pixiObj.position.x = collider.owner.pixiObj.getBounds().left - this.owner.pixiObj.width;
                else if (deltaPos.x < -10e-6)
                    this.owner.pixiObj.position.x = collider.owner.pixiObj.getBounds().right;
                else {
                    this.owner.pixiObj.position.x = oldPosX;
                }
                this._velocity.x = 0;
            }

            let oldPosY = this.owner.pixiObj.position.y;
            this.owner.pixiObj.position.y += deltaPos.y;
            collider = this.collisionManager.collidePhysics(this.owner)
            if (collider != null) {
                if (deltaPos.y > 10e-6)
                    this.owner.pixiObj.position.y = collider.owner.pixiObj.getBounds().top - this.owner.pixiObj.height;
                else if (deltaPos.y < -10e-6)
                    this.owner.pixiObj.position.y = collider.owner.pixiObj.getBounds().bottom ;
                else {
                    this.owner.pixiObj.position.y = oldPosY;
                }
                this._velocity.y = 0;
            }
        }

    }

    applyVelocity(delta: number) {
        const gravityDelta = this._gravity * GRAVITY_MODIFIER;
        this._velocity.x = this._velocity.x * this.aceleration.x + this._force.x;
        this._velocity.y = (this._velocity.y - gravityDelta) * this.aceleration.y + this._force.y + gravityDelta;
        this._force = this._force.multiply(0);
        if (Math.abs(this._velocity.x) < 10e-1)
            this._velocity.x = 0;
        if (Math.abs(this._velocity.y) < 10e-1)
            this._velocity.y = 0;
    }

    calcPositionChange(delta: number): ECSA.Vector {
        return this._velocity.multiply(delta * 0.001);
    }

    move(vector: ECSA.Vector) {
        this.owner.pixiObj.position.x = vector.x;
        this.owner.pixiObj.position.y = vector.y;
    }

    force(vector: ECSA.Vector) {
        this._force = this._force.add(vector);
    }
}