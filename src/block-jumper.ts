import * as ECSA from '../libs/pixi-component';
import GameFactory from './game-factory';
import { Assets, SCENE_HEIGHT, SCENE_WIDTH } from './constants';

class BlockJumper {
  engine: ECSA.GameLoop;

  constructor() {
    this.engine = new ECSA.GameLoop();
    let canvas = (document.getElementById('gameCanvas') as HTMLCanvasElement);

    // init the game loop
    this.engine.init(canvas, SCENE_WIDTH, SCENE_HEIGHT, 1, // width, height, resolution
      {
       flagsSearchEnabled: true, // searching by flags feature
       statesSearchEnabled: false, // searching by states feature
       tagsSearchEnabled: true, // searching by tags feature
       namesSearchEnabled: true, // searching by names feature
       notifyAttributeChanges: false, // will send message if attributes change
       notifyStateChanges: false, // will send message if states change
       notifyFlagChanges: false, // will send message if flags change
       notifyTagChanges: false, // will send message if tags change
       debugEnabled: false // debugging window
     }, true); // resize to screen

    this.engine.app.loader
      .reset()
      //.add(myFile, 'myFileUrl') load your assets here
      .add(Assets.DATA, './assets/config.json')
      .add(Assets.SND_BACKGROUND, './assets/background.mp3')
      .add(Assets.SND_WIN, './assets/win.mp3')
      .add(Assets.SND_c6, './assets/notes/c6.mp3')
      .add(Assets.SND_d6, './assets/notes/d6.mp3')
      .add(Assets.SND_e6, './assets/notes/e6.mp3')
      .add(Assets.SND_f6, './assets/notes/f6.mp3')
      .add(Assets.SND_g6, './assets/notes/g6.mp3')
      .add(Assets.SND_a6, './assets/notes/a6.mp3')
      .add(Assets.SND_h6, './assets/notes/b6.mp3')
      .add(Assets.SND_c5, './assets/notes/c5.mp3')
      .add(Assets.SND_d5, './assets/notes/d5.mp3')
      .add(Assets.SND_e5, './assets/notes/e5.mp3')
      .add(Assets.SND_f5, './assets/notes/f5.mp3')
      .add(Assets.SND_g5, './assets/notes/g5.mp3')
      .add(Assets.SND_a5, './assets/notes/a5.mp3')
      .add(Assets.SND_h5, './assets/notes/b5.mp3')
      .load(() => this.onAssetsLoaded());
  }

  onAssetsLoaded() {
    // init the scene and run your game
    let factory = new GameFactory();
    factory.resetGame(this.engine.scene);
  }
}

// this will create a new instance as soon as this file is loaded
export default new BlockJumper();