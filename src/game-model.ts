export class GameModel {
    // ========================= dynamic data
 
    // =========================
  
  
    // ========================= static data
    gravity;
    spawning_frequency;
  
  
    /**
     * Loads model from JSON structure
     */
    loadModel(data: any) {
      this.gravity = data.gravity;
      this.spawning_frequency = data.spawning_frequency;
    }
  
    /**
     * Resets dynamic data
     */
    reset() {
    }
  }