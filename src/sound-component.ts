import * as ECSA from '../libs/pixi-component';
import { Messages, Assets } from './constants';
import PIXISound from 'pixi-sound';

export class SoundComponent extends ECSA.Component {

    notes;
    constructor() {
        super()
        this.notes = [
            Assets.SND_c6,
            Assets.SND_d6,
            Assets.SND_e6,
            Assets.SND_f6,
            Assets.SND_g6,
            Assets.SND_a6,
            Assets.SND_h6,
            Assets.SND_c5,
            Assets.SND_d5,
            Assets.SND_e5,
            Assets.SND_f5,
            Assets.SND_g5,
            Assets.SND_a5,
            Assets.SND_h5,]
    }

    onInit() {
        super.onInit();
        this.subscribe(Messages.GAME_OVER, Messages.GAME_START, Messages.PLAY_TONE, Messages.GAME_RESTART);
    }

    onMessage(msg: ECSA.Message) {
        if (msg.action === Messages.GAME_START)
            PIXISound.play(Assets.SND_BACKGROUND);
        if (msg.action === Messages.PLAY_TONE) {
            let random = Math.floor(Math.random() * this.notes.length);
            PIXISound.play(this.notes[random]);
        }
        if (msg.action === Messages.GAME_OVER) {
            PIXISound.stop(Assets.SND_BACKGROUND)
            PIXISound.play(Assets.SND_WIN);
        }
        if (msg.action === Messages.GAME_RESTART) {
            PIXISound.stop(Assets.SND_BACKGROUND)
        }
    }
}
