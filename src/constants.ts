export enum Assets {
  DATA = 'data_json',
  SND_BACKGROUND = 'background',
  SND_WIN = 'win',
  SND_c6 = 'c6',
  SND_d6 = 'd6',
  SND_e6 = 'e6',
  SND_f6 = 'f6',
  SND_g6 = 'g6',
  SND_a6 = 'a6',
  SND_h6 = 'b6',
  SND_c5 = 'c5',
  SND_d5 = 'd5',
  SND_e5 = 'e5',
  SND_f5 = 'f5',
  SND_g5 = 'g5',
  SND_a5 = 'a5',
  SND_h5 = 'b5',
}

export enum Attributes {
  GAME_MODEL = 'game-model',
  GAME_FACTORY = 'game-factory',
  COLLISION_MANAGER = 'collisionmanager'
}

export enum Messages {
  GAME_OVER = 'gameover',
  GAME_START = 'gamestart',
  PLAY_TONE = 'playtone',
  GAME_RESTART = 'gamerestart'
}

export enum Names {
  PLAYER = 'player',
  PLAYER_BODY = 'player_body',
  PLAYER_CONTAINER = 'player_container',
  PLAYER_FOOT = 'player_foot',
  PLAYER_HEAD = 'player_head',
  BLOCK = 'block',
  FINISH = 'finish'
}

export enum Tags {
  COLLIDER = 'COLLIDER',
}


export enum BFlags {
  BLOCK = 1,
  PICKUP = 2,
  COLIDER = 3
}


export enum ComponentNames {
  COLLIDER = 'collider',
  DYNAMICS = 'dynamics',
  PLAYER_CONTROLLER = 'playercontroller',
  LOBBY_CONTROLLER = 'lobbycontroller'
}

export const SCENE_WIDTH = 800;
export const SCENE_HEIGHT = 600;
export const BLOCK_SIZE = 50;
export const GRAVITY_MODIFIER = 300;
export const ACCELERATION_MODIFIER = 0.01