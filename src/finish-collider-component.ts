import { RectangleCollider } from './abstracts/rectangle-collider';
import { Names, Attributes, Messages } from './constants';
import GameFactory from './game-factory';
import { GameModel } from './game-model';


export class FinishCollider extends RectangleCollider {

    triggered: boolean;
    onInit() {
        super.onInit();
        this.isTrigger = true;
        this.triggered = false;
    }


    public onTrigger(other: RectangleCollider) {
        if (other.owner.name === Names.PLAYER && !this.triggered) {
            this.sendMessage(Messages.GAME_OVER);
            this.triggered = true;
        }
    }
}