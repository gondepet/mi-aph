# Block Jumper

Block jumper je jednoduchá skákací hra, kde je cílem se dostat do horní zelené části obrazovky.

Ovládání hry:
- ← - Pohyb doleva
- → - Pohyb doprava
- ↑ - Skok
- Mezerník - Začátek hry
- R - Restart hry

Hudba a zvukové efekty byly vytvořeny pomocí [BOSCA CEOIL](https://boscaceoil.net/).

Mnou známý, ale ne mnou dosažený, nejrychlejší čas je 13.79s. 

![Screenshot](https://gitlab.fit.cvut.cz/gondepet/mi-aph/raw/master/documentation/Screenshot.PNG "Screenshot")


## Architektura hry

Hra využívá ECSA pattern. Komponenty si předávají informace pomocí Messages a identifikují se navzájem pomocí Tags, ComponentName, nebo BFlags.

Hra má napsanou vlastní jednoduchou fyziku s gravitací a s obdélníkovými kolidery a triggery. Fyzika je řešena v komponentně Dynamics a kolize jsou ovládány v CollisionManager, který vyhledává všechny objekty s tagem Collider a bere si jejich RectangleCollider. Pro správné fungování componenty Dynamics je nutné přidat objektu komponentu Collider, aby bylo možné detekovat narážení do ostatních objektů. Hra pracuje s relativné malým množství objektů, a proto nejsou kolize optimalizované.

Hra má v assetech soubor config, ve kterém lze měnit gravitační konstantu a rychlost vytváření nových bloků.

![Architecture diagram](https://gitlab.fit.cvut.cz/gondepet/mi-aph/raw/master/documentation/architecture.png "Architecture Diagram")

