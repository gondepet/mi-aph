import * as ECSA from '../libs/pixi-component';
import { RectangleCollider } from './abstracts/rectangle-collider';
import { Names, ComponentNames, BFlags, Messages } from './constants';
import Dynamics from './abstracts/dynamics';


export class BlockCollider extends RectangleCollider {

    public onCollision(other: RectangleCollider) {
        let cmp = this.owner.findComponentByName<Dynamics>(ComponentNames.DYNAMICS);
        cmp.disabled = true;
    }
}