import * as ECSA from '../libs/pixi-component';
import { GameModel } from './game-model';
import { Assets, Attributes, Names, ComponentNames, BFlags, SCENE_WIDTH, SCENE_HEIGHT, BLOCK_SIZE, Tags } from './constants';
import PlayerInputController from './player-controller-component';
import { PlayerFootCollider } from './player-foot-collider-component';
import Dynamics from './abstracts/dynamics';
import { RectangleCollider } from './abstracts/rectangle-collider';
import { CollisionManager } from './colision-manager';
import { BlockSpawner } from './block-spawner';
import { BlockCollider } from './block-collider-component';
import { FinishCollider } from './finish-collider-component';
import { Timewatch as Stopwatch } from './timewatch-component';
import { SoundComponent } from './sound-component';
import LobbyController from './lobby-controller-component';

export default class GameFactory {
    initializeGame(rootObject: ECSA.GameObject, model: GameModel) {

        let scene = rootObject.scene;
        let builder = new ECSA.Builder(scene);
        let collisionManager = new CollisionManager();
        scene.assignGlobalAttribute(Attributes.COLLISION_MANAGER, collisionManager);

        // add root components
        builder
            .withComponent(new ECSA.KeyInputComponent())
            .withComponent(new BlockSpawner())
            .withComponent(collisionManager)
            .withComponent(new SoundComponent())
            .buildInto(rootObject);
        rootObject.asContainer().sortableChildren = true;

        //Spawn walls
        let finish = builder.asGraphics(Names.FINISH)
            .relativePos(0, 0)
            .anchor(0.5, 0.5)
            .withComponent(new FinishCollider())
            .withParent(rootObject)
            .withTag(Tags.COLLIDER)
            .build();
        finish.asGraphics().beginFill(0x00ff00);//0x006b1e
        finish.asGraphics().drawRect(BLOCK_SIZE, 0, SCENE_WIDTH - (BLOCK_SIZE * 2), BLOCK_SIZE - 1);
        finish.asGraphics().alpha = 0.35;
        finish.asGraphics().zIndex = 1;
        finish.asGraphics().endFill();

        builder.relativePos(0.5, 0.035).anchor(0.5, 0.5)
            .asText("Finish", 'Finish', new PIXI.TextStyle({
                fill: "white",
                fontSize: 18
            }))
            .withParent(rootObject)
            .build().asText().zIndex = 100;
        this.spawnWalls(rootObject);

        // create player
        let playerContainer = builder
        .asContainer(Names.PLAYER_CONTAINER)
        .withParent(rootObject)
        .build();

        let player = builder
            .asGraphics(Names.PLAYER)
            .relativePos(0.5, 0.7)
            .anchor(0.5, 0.5)
            .withParent(playerContainer)
            .withTag(Tags.COLLIDER)
            .withComponent(new RectangleCollider())
            .withComponent(new PlayerInputController())
            .withComponent(new Dynamics(new ECSA.Vector(0.95, 0.95), new ECSA.Vector(0, model.gravity * 10)))
            .build();
        player.asGraphics().beginFill(0xFFFFFF);
        player.asGraphics().drawRoundedRect(0, 0, 20, 20, 3);

        player.asGraphics().endFill();
        let playerFoot = builder
            .asGraphics(Names.PLAYER_FOOT)
            .relativePos(0.512 , 0.734)
            .anchor(0.5, 0.5)
            .withParent(playerContainer)
            .withTag(Tags.COLLIDER)
            .withComponent(new PlayerFootCollider(player))
            .build();
        playerFoot.asGraphics().beginFill(0xFF0000);
        playerFoot.asGraphics().drawRect(0, 0, 2, 2);
        playerFoot.asGraphics().alpha = 0;
        playerFoot.asGraphics().endFill();


        // DEBUG Player info
        /*let debugText = builder.relativePos(0.08, 0.98).anchor(0, 1)
            .withComponent(new ECSA.GenericComponent('PlayerDebugText').doOnUpdate((cmp, delta, absolute) => {
                cmp.owner.asText().text = 'Player' +
                    '\npos.x: ' + player.pixiObj.position.x +
                    '\npos.y: ' + player.pixiObj.position.y +
                    '\nvelocity.x: ' + player.findComponentByName<Dynamics>(ComponentNames.DYNAMICS).velocity.x +
                    '\nvelocity.y: ' + player.findComponentByName<Dynamics>(ComponentNames.DYNAMICS).velocity.y +
                    '\nwidth: ' + player.asGraphics().pixiObj.getBounds().width +
                    '\nheight: ' + player.asGraphics().pixiObj.getBounds().height;
            }))
            .asText("PlayerDebug", '', new PIXI.TextStyle({
                fill: "red",
                fontSize: 12
            }))
            .withParent(rootObject)
            .build();
        debugText.asText().zIndex = 10;*/

        // time;
        let timeWatch = builder.relativePos(0.01, 0.98).anchor(0, 1)
            .withComponent(new Stopwatch())
            .asText("stopwatch", '', new PIXI.TextStyle({
                fill: "white",
                fontSize: 20
            }))
            .withParent(rootObject)
            .build();
        timeWatch.asText().zIndex = 10;

        this.gameLobby(rootObject);
    }

    resetGame(scene: ECSA.Scene) {
        scene.clearScene();
        let model = new GameModel();
        model.loadModel(scene.app.loader.resources[Assets.DATA].data);
        scene.assignGlobalAttribute(Attributes.GAME_FACTORY, this);
        scene.assignGlobalAttribute(Attributes.GAME_MODEL, model);
        scene.invokeWithDelay(0, () => this.initializeGame(scene.stage, model));
    }

    createBlock(owner: ECSA.GameObject) {
        const MAX_BLOCKS_ON_SCREEN = SCENE_WIDTH / BLOCK_SIZE;
        let root = owner.scene.stage;
        let color = Math.random() * 0xFFFFFF;
        let posY = -0.2;
        let posX = (Math.floor(Math.random() * (MAX_BLOCKS_ON_SCREEN - 2)) + 1) / MAX_BLOCKS_ON_SCREEN;
        let obj = new ECSA.Builder(owner.scene)
            .asGraphics(Names.BLOCK)
            .relativePos(posX, posY)
            .anchor(0, 0)
            .withParent(root)
            .withTag(Tags.COLLIDER)
            .withComponent(new BlockCollider())
            .withComponent(new Dynamics(new ECSA.Vector(1, 1.002), new ECSA.Vector(0, 10)))
            .withFlag(BFlags.BLOCK)
            .build();

        obj.asGraphics().beginFill(0x000000);
        obj.asGraphics().drawRect(0, 0, BLOCK_SIZE, BLOCK_SIZE);
        obj.asGraphics().endFill();
        obj.asGraphics().beginFill(color);
        obj.asGraphics().drawRect(1, 1, BLOCK_SIZE - 2, BLOCK_SIZE - 2);
        obj.asGraphics().endFill();
        obj.asGraphics().zIndex = -1;
    }

    spawnWalls(owner: ECSA.GameObject) {
        const color = 0x747474;
        for (let i = 0; i < SCENE_HEIGHT / BLOCK_SIZE; i++) {
            let posY = (BLOCK_SIZE / SCENE_HEIGHT) * i;
            let objRight = new ECSA.Builder(owner.scene)
                .asGraphics(Names.BLOCK)
                .relativePos(1 - (BLOCK_SIZE / SCENE_WIDTH), posY)
                .anchor(0, 0)
                .withParent(owner.scene.stage)
                .withComponent(new RectangleCollider())
                .withTag(Tags.COLLIDER)
                .withFlag(BFlags.BLOCK)
                .build();


            objRight.asGraphics().beginFill(0x000000);
            objRight.asGraphics().drawRect(0, 0, BLOCK_SIZE, BLOCK_SIZE);
            objRight.asGraphics().endFill();
            objRight.asGraphics().beginFill(color);
            objRight.asGraphics().drawRect(1, 1, BLOCK_SIZE - 2, BLOCK_SIZE - 2);
            objRight.asGraphics().endFill();

            let objLeft = new ECSA.Builder(owner.scene)
                .asGraphics(Names.BLOCK)
                .relativePos(0, posY)
                .anchor(0, 0)
                .withParent(owner.scene.stage)
                .withComponent(new RectangleCollider())
                .withTag(Tags.COLLIDER)
                .withFlag(BFlags.BLOCK)
                .build();

            objLeft.asGraphics().beginFill(0x000000);
            objLeft.asGraphics().drawRect(0, 0, BLOCK_SIZE, BLOCK_SIZE);
            objLeft.asGraphics().endFill();
            objLeft.asGraphics().beginFill(color);
            objLeft.asGraphics().drawRect(1, 1, BLOCK_SIZE - 2, BLOCK_SIZE - 2);
            objLeft.asGraphics().endFill();
        }
        for (let i = 1; i < SCENE_WIDTH / BLOCK_SIZE - 1; i++) {
            let posX = (BLOCK_SIZE / SCENE_WIDTH) * i;
            let objBottom = new ECSA.Builder(owner.scene)
                .asGraphics(Names.BLOCK)
                .relativePos(posX, 1 - (BLOCK_SIZE / SCENE_HEIGHT))
                .anchor(0, 0)
                .withParent(owner.scene.stage)
                .withComponent(new RectangleCollider())
                .withTag(Tags.COLLIDER)
                .withFlag(BFlags.BLOCK)
                .build();

            objBottom.asGraphics().beginFill(0x000000);
            objBottom.asGraphics().drawRect(0, 0, BLOCK_SIZE, BLOCK_SIZE);
            objBottom.asGraphics().endFill();
            objBottom.asGraphics().beginFill(color);
            objBottom.asGraphics().drawRect(1, 1, BLOCK_SIZE - 2, BLOCK_SIZE - 2);
            objBottom.asGraphics().endFill();
        }
    }

    gameFinished(time: string, owner: ECSA.GameObject) {
        let builder = new ECSA.Builder(owner.scene);
        builder.relativePos(0.5, 0.45).anchor(0.5, 0.5)
            .asText("finishedtime", time, new PIXI.TextStyle({
                fill: "white",
                fontSize: 26
            }))
            .withParent(owner)
            .build();
        builder.relativePos(0.5, 0.5).anchor(0.5, 0.5)
            .asText("GameFinished", 'Game Finished', new PIXI.TextStyle({
                fill: "white",
                fontSize: 22
            }))
            .withParent(owner)
            .build();
        builder.relativePos(0.5, 0.55).anchor(0.5, 0.5)
            .asText("PressF", 'press F to pay respect', new PIXI.TextStyle({
                fill: "white",
                fontSize: 18
            }))
            .withParent(owner)
            .build();
    }

    gameLobby(owner: ECSA.GameObject) {
        let builder = new ECSA.Builder(owner.scene);
        let container = builder
            .asContainer("Lobby")
            .relativePos(0.5, 0.5)
            .anchor(0.5, 0.5)
            .withComponent(new LobbyController())
            .withParent(owner)
            .build();

        builder
            .relativePos(0.5, 0.4)
            .anchor(0.5, 0.5)
            .asText("blockJumper", "BLOCK JUMPER", new PIXI.TextStyle({
                fill: "white",
                fontSize: 35
            }))
            .withParent(container)
            .build();

        builder
            .relativePos(0.5, 0.5)
            .anchor(0.5, 0.5)
            .asText("pressspace", 'Press SPACE to start', new PIXI.TextStyle({
                fill: "white",
                fontSize: 22
            }))
            .withParent(container)
            .build();
        builder.relativePos(0.5, 0.55).anchor(0.5, 0.5)
            .asText("pressR", 'press R to restart', new PIXI.TextStyle({
                fill: "white",
                fontSize: 18
            }))
            .withParent(container)
            .build();
    }
}