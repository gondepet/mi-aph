import * as ECSA from '../libs/pixi-component';
import { RectangleCollider } from './abstracts/rectangle-collider';
import { Names, ComponentNames, BFlags, Messages } from './constants';
import Dynamics from './abstracts/dynamics';
import PlayerInputController from './player-controller-component';

export class PlayerFootCollider extends RectangleCollider {

    playerController: PlayerInputController;

    private _player: ECSA.GameObject;
    private _playerInputController: PlayerInputController;
    private _playerDynamics: Dynamics;

    constructor(player: ECSA.GameObject) {
        super();
        this._player = player;
    }
    onInit() {
        this.isTrigger = true;
        this._playerInputController = this._player.findComponentByName<PlayerInputController>(ComponentNames.PLAYER_CONTROLLER);
        this._playerDynamics = this._player.findComponentByName<Dynamics>(ComponentNames.DYNAMICS);
    }

    onUpdate() {
        this.owner.pixiObj.position.x = this._playerDynamics.position.x + this._player.pixiObj.width / 2 - 1;
        this.owner.pixiObj.position.y = this._playerDynamics.position.y + this._player.pixiObj.height;
    }

    public onTrigger(other: RectangleCollider) {
        if (other.owner.hasFlag(BFlags.BLOCK)) {
            if (this._playerInputController.jumping === true)
                this.sendMessage(Messages.PLAY_TONE);
            this._playerInputController.jumping = false;
        }
    }

}
